export default [
    {id: 1, name: 'BMW', price: 10 },
    {id: 2, name: 'Google', price: 220 },
    {id: 3, name: 'Apple', price: 330 },
    {id: 4, name: 'Twitter', price: 40 },
    {id: 5, name: 'Facebook', price: 50 },
]